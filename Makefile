# Dragon Booty
# 2017 Ciaran Anscomb
# Public Domain

.PHONY: all
all: booty.dsk

PACKAGE = booty
VERSION = 1.0
distdir = $(PACKAGE)-$(VERSION)

####

ASM6809 = asm6809 -v
CLEAN =
EXTRA_DIST =

####

# Boot block.  Same image is used for DragonDOS and RSDOS disks.

booty.bin: booty.s
	$(ASM6809) -B -l $(@:.bin=.lis) -o $@ $<

CLEAN += booty.lis booty.bin

# Rules to create the disk image.

booty.dsk: booty.bin
	rm -f $@
	cp hybrid.dsk $@
	dd if=booty.bin of=$@ bs=256 seek=2 conv=notrunc
	dd if=booty.bin of=$@ bs=256 seek=$(shell expr 34 \* 18) conv=notrunc

CLEAN += booty.dsk

####

EXTRA_DIST += booty.dsk

.PHONY: dist
dist: $(EXTRA_DIST)
	git archive --format=tar --output=./$(distdir).tar --prefix=$(distdir)/ HEAD
	tar -r -f ./$(distdir).tar --owner=root --group=root --mtime=./$(distdir).tar --transform "s,^,$(distdir)/," $(EXTRA_DIST)
	gzip -f9 ./$(distdir).tar

CLEAN += $(distdir).tar $(distdir).tar.gz

####

.PHONY: clean
clean:
	rm -f $(CLEAN)
