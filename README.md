# Dragon Booty

Constructs hybrid disk from template including DragonDOS and RSDOS
filesystems.

Assembles and installs unified boot block to two locations:

 * DragonDOS - track 0, sector 3
 * RSDOS - track 34, sector 1

Boot sector uses tokenised BASIC to load and run appropriate file from:

 * DRAGON.BIN - DragonDOS
 * COCO12.BIN - RSDOS, CoCo 1/2
 * COCO3.BIN  - RSDOS, CoCo 3

Source contains examples for starting a BASIC program instead.
