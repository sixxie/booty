; Dragon Booty
; 2017 Ciaran Anscomb
; Public Domain

; Boot block, compatible with both Dragon and Tandy CoCo.  Copies a run of
; tokenised BASIC into low RAM, sets up the requisite pointers for BASIC
; to consider them as the next commands to execute, then returns to the
; OS.
;
; DragonDOS loads this from Track 0, Sector 3.  RSDOS loads it from Track
; 34, Sector 1.

		org $2600
		setdp 0		; assumed

boot_flag	fcc /OS/

boot_exec	ldx #run_dragon
		lda $a000	; [$A000] points to ROM1 in CoCos
		anda #$20
		beq 10F		; dragon
		ldx #run_coco2
		lda $80fd
		cmpa #'2
		bne 10F
		ldx #run_coco3
10		ldu #$02dc
		stu $00a6
20		lda ,x+
		sta ,u+
		bne 20B
		clr boot_flag	; needed for coco
		; work around DOSplus issue where BOOT does not
		; expect to return
		ldx ,s
		ldd 1,x
		cmpd #$8344	; looks like jump to error handler?
		bne 30F
		leas 2,s
30		rts

; DragonDOS can use RUN token ($8f) for both binary and BASIC programs.

run_dragon	fcc /:/
		fcc $8f,/"DRAGON.BIN":/,0
		;fcc $8f,/"DRAGON.BAS":/,0

; RSDOS can use RUN token ($8e) for BASIC, but must LOADM ($d3 'M')
; followed by EXEC ($a2) for binary.

run_coco2	fcc /:/
		fcc $d3,/M"COCO12.BIN":/,$a2,0
		;fcc $8e,/"COCO12.BAS":/,0

run_coco3	fcc /:/
		fcc $d3,/M"COCO3.BIN":/,$a2,0
		;fcc $8e,/"COCO3.BAS":/,0

		; fill rest of sector
		rzb (256-(*%256))%256
